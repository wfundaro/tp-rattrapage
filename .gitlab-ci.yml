image: node:latest

stages:
  - analysis
  - install_dependencies
  - prettier
  - lint
  - test
  - after-test
  - doc
  - after-doc
  - message

cache:
  paths:
    - node_modules/

install_dependencies:
  stage: install_dependencies
  script:
    - npm install
  artifacts:
    paths:
      - node_modules/
  except:
    - schedules

sonar:analysis:
  image: sonarsource/sonar-scanner-cli:latest
  stage: analysis
  script:
    - sonar-scanner -Dsonar.projectKey=$SONAR_PROJECT -Dsonar.sources=. -Dsonar.host.url=$SONAR_URL -Dsonar.login=$SONAR_KEY -Dsonar.exclusions=**/node_modules/**/*
  when: always
  only:
    - schedules

prettier:
  stage: prettier
  script:
    # - npm i
    - npm run prettier
  only:
    - develop
    - /^feature/.*/
  except:
    - tags
    - schedules

lint:
  stage: lint
  script:
    # - npm i
    - npm run lint
  only:
    - develop
    - /^feature/.*/
  except:
    - tags
    - schedules

test:unit:
  stage: test
  script:
    # - npm i
    - npm run test
  only:
    - develop
  except:
    - tags
    - schedules
  artifacts:
    paths:
      - jest_html_reporters.html
    when: always

.deploy:
  image: kroniak/ssh-client
  script:
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa

deploy:report:
  extends:
    - .deploy
  stage: after-test
  when: always
  needs:
    - test:unit
  only:
    - develop
  except:
    - schedules
  after_script:
    - ssh $SSH_USER@$SSH_SERVER_IP -p $SSH_PORT "mkdir -p $SSH_DIR/test"
    - scp -P $SSH_PORT ./jest_html_reporters.html $SSH_USER@$SSH_SERVER_IP:$SSH_DIR/test/jest_html_reporters-$(date +%Y%m%d).html

send:slack:after:test:
  stage: after-test
  image: kaylleur/test-ynov:latest
  when: on_failure
  needs:
    - test:unit
  only:
    - develop
  except:
    - tags
    - schedules
  variables:
    SLACK_WEBHOOK: $SLACK_WEBHOOK
    SLACK_CHANNEL: $SLACK_CHANNEL
  script:
    - export SLACK_MESSAGE="$WEB_URL/test/jest_html_reporters-$(date +%Y%m%d).html  $CI_PROJECT_NAME->$GITLAB_USER_NAME test failed on commit-> $CI_COMMIT_TITLE / [$CI_COMMIT_SHA]"
    - cd /opt/script && ./script.sh

send:slack:
  stage: message
  image: kaylleur/test-ynov:latest
  when: on_success
  only:
    - master
    - tags
  variables:
    SLACK_WEBHOOK: $SLACK_WEBHOOK_WILL
    SLACK_CHANNEL: $SLACK_WILL_CHANNEL
  script:
    - if [ $CI_COMMIT_REF_SLUG = "master" ]; then CI_COMMIT_REF_SLUG=latest; fi;
    - if [ $CI_COMMIT_REF_SLUG = "tags" ]; then CI_COMMIT_REF_SLUG=tags; fi;
    - export SLACK_MESSAGE="$CI_PROJECT_NAME prod->$CI_COMMIT_REF_SLUG doc url:$WEB_URL/prod/docs/"
    - cd /opt/script && ./script.sh

doc:api:
  stage: doc
  script:
    # - npm i
    - npm run apidoc
  only:
    - develop
    - master
    - tags
  except:
    - schedules
  artifacts:
    paths:
      - docs/**/*
    when: on_success

deploy:apidoc:
  extends:
    - .deploy
  stage: after-doc
  except:
    - schedules
  needs:
    - doc:api
  only:
    - tags
    - master
  after_script:
    - if [ $CI_COMMIT_REF_SLUG = "master" ]; then CI_COMMIT_REF_SLUG=latest; fi;
    - ssh $SSH_USER@$SSH_SERVER_IP -p $SSH_PORT "mkdir -p $SSH_DIR/prod/docs"
    - scp -r -P $SSH_PORT ./docs $SSH_USER@$SSH_SERVER_IP:$SSH_DIR/prod/

deploy:apidocdev:
  extends:
    - .deploy
  stage: after-doc
  except:
    - schedules
  needs:
    - doc:api
  only:
    - develop
  after_script:
    - ssh $SSH_USER@$SSH_SERVER_IP -p $SSH_PORT "mkdir -p $SSH_DIR/dev/docs"
    - scp -r -P $SSH_PORT ./docs $SSH_USER@$SSH_SERVER_IP:$SSH_DIR/dev/
